import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportesRoutingModule } from './reportes-routing.module';
import { ReporteViewComponent } from './reporte-view/reporte-view.component';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { PaginatorModule } from 'primeng/paginator';
import { CalendarModule } from 'primeng/calendar';


@NgModule({
  declarations: [ReporteViewComponent],
  imports: [
    CommonModule,
    ReportesRoutingModule,
    TableModule,
    ButtonModule,
    PaginatorModule,
    CalendarModule
  ]
})
export class ReportesModule { }
