import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReporteViewComponent } from './reporte-view/reporte-view.component';

const routes: Routes = [
  { path: "", component: ReporteViewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportesRoutingModule { }
