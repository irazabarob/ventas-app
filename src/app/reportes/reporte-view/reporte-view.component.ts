import { Component, OnInit } from '@angular/core';
import { ReporteService } from 'src/app/servicios/reporte.service';

@Component({
  selector: 'app-reporte-view',
  templateUrl: './reporte-view.component.html',
  styleUrls: ['./reporte-view.component.scss']
})
export class ReporteViewComponent implements OnInit {

  first = 0;

  rows = 10;

  ventasMensuales : any;
  fechas!: Date[];
  
  constructor(private reporteService: ReporteService) { }

  ngOnInit(): void {
    this.cargarDatos();
  }

  cargarDatos(){
 
  }

  filtrarFechas(){
    console.log(this.fechas[0]);
    this.reporteService.reportePorMes({fecha1:this.fechas[0], fecha2: this.fechas[1]}).subscribe( data => {
      this.ventasMensuales = data;     
    })
  }

  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage()  {
    // return this.customers ? this.first === (this.customers.length - this.rows) : true;
  }

  isFirstPage() {
    // return this.customers ? this.first === 0 : true;
  }

}
