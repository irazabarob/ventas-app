import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteViewComponent } from './reporte-view.component';

describe('ReporteViewComponent', () => {
  let component: ReporteViewComponent;
  let fixture: ComponentFixture<ReporteViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReporteViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
