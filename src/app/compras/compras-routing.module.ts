import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompraViewComponent } from './components/compra-view/compra-view.component';

const routes: Routes = [
  { path: "", component: CompraViewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComprasRoutingModule { }
