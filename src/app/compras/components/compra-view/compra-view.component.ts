import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { SelectItem } from 'primeng/api';
import { Compra } from 'src/app/models/compra';
import { Producto } from 'src/app/models/producto';
import { selectProductos } from 'src/app/productos/store/producto.selector';

import * as fromCompraActions from "../../store/compras.actions";

@Component({
  selector: 'app-compra-view',
  templateUrl: './compra-view.component.html',
  styleUrls: ['./compra-view.component.scss']
})
export class CompraViewComponent implements OnInit {

  productos: SelectItem[] = [];
  listProducto!: Producto[];

  ventaTotal: number = 0;
  //variables para preparar data a vender
  productosAComprar: Producto[] = [];
  listaCompra: Compra[] = [];


  productoForm: FormGroup = new FormGroup({});

  constructor(private productoStore: Store<Producto>,
    private compraStore: Store<Compra>,
    private fb: FormBuilder,
    ) { }

  ngOnInit(): void {
    this.cargarDatosProductos();
    this.inicializarFormProd();
  }

  inicializarFormProd() {
    this.productoForm = this.fb.group({
      idProducto: [""],
      codigo: [""],
      descripcion: [""],
      marca: [""],
      stock: [""],
      precioVenta: [""],
      cantidad: [""]
    })
  }

  cargarDatosProductos() {
    this.productoStore.pipe(select(selectProductos)).subscribe(listProduct => {
      this.listProducto = listProduct;
      listProduct.forEach(p => {
        this.productos.push({ label: p.codigo, value: p.idProducto })
      })
    })
  }

  select() {
    let prod = this.listProducto.filter(p => this.productoForm.controls.idProducto.value == p.idProducto);

    this.productoForm.controls.idProducto.setValue(prod[0].idProducto);
    this.productoForm.controls.codigo.setValue(prod[0].codigo);
    this.productoForm.controls.precioVenta.setValue(prod[0].precioVenta);
    this.productoForm.controls.stock.setValue(prod[0].stock);
    this.productoForm.controls.descripcion.setValue(prod[0].descripcion);
  }

  addToCompra() {
    let prod = this.productoForm.value;
    prod.stock = prod.cantidad;
    this.productosAComprar.push(prod);

    this.ventaTotal += prod.precioVenta * prod.cantidad;
    this.inicializarFormProd();
  }

  registarVenta() {
    let fech = new Date();
    let compra = {    
      fecha: fech,
      // fecha: `${fech.getFullYear()}-${fech.getMonth() + 1}-${fech.getDate()}`,
      listProducto: this.productosAComprar
    };

    //aqui iria la compra con su lista de productos
    
    this.compraStore.dispatch(fromCompraActions.registrarCompra({compra: compra}))
    // this.ventaStore.dispatch(fromVentaActions.registrarVenta({ venta: venta as Venta, dv: this.listaCompra }));

    setTimeout(() => {
      this.inicializarFormProd();
      this.productosAComprar = [];
      this.listaCompra = [];
    },500);
  }

}
