import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComprasRoutingModule } from './compras-routing.module';
import { CompraViewComponent } from './components/compra-view/compra-view.component';
import { DropdownModule } from 'primeng/dropdown';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';
import { MessageService } from 'primeng/api';
import { StoreModule } from '@ngrx/store';
import { comprasReducer, compraStateFeatureKey } from './store/compras.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ComprasEffects } from './store/compras.effects';


@NgModule({
  declarations: [CompraViewComponent],
  imports: [
    CommonModule,
    ComprasRoutingModule,
    DropdownModule,
    ReactiveFormsModule,
    ButtonModule,
    ToastModule,
    TableModule,
    StoreModule.forFeature(compraStateFeatureKey, comprasReducer),
    EffectsModule.forFeature([ComprasEffects])
  ],
  providers: [ MessageService ]
})
export class ComprasModule { }
