import { createAction, props } from "@ngrx/store";
import { Compra } from "src/app/models/compra";

export const registrarCompra = createAction(
    "[Compras] Registrar Compras",
    props<{compra: Compra}>()
)

export const registarCompraSucess = createAction(
    "[Compras] Registrar Compra Success",
    props<{compra: Compra}>()
)

export const registrarCompraFail = createAction(
    "[Compras] Registrar Compra Fail",
    props<{error: any}>()
)