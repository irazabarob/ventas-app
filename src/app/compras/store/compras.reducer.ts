import { Action, createReducer, on } from "@ngrx/store";
import { Compra } from "src/app/models/compra";

import * as fromCompraActions from "./compras.actions";

export const compraStateFeatureKey = "comprasState";

export interface ComprasState {
    compras: Compra[];
    error: any;
}

export const initialState: ComprasState = {
    compras: [],    
    error: undefined
}

export const _compraReducer = createReducer(
    initialState,
    on(fromCompraActions.registarCompraSucess, (state, action) => {
        return {
            compras: [...state.compras, action.compra],
            error: undefined
        }
    })
)

export function comprasReducer(state: ComprasState | undefined, action: Action){
    return _compraReducer(state, action);
}
