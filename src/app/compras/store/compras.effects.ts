import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { MessageService } from "primeng/api";
import { of } from "rxjs";
import { catchError, map, mergeMap } from "rxjs/operators";
import { CompraService } from "src/app/servicios/compra.service";

import * as fromCompraActions from "./compras.actions";

@Injectable()
export class ComprasEffects {
    constructor(private compraService: CompraService,
                private actions$: Actions,
                private messageService: MessageService){}

    //REGISTRAR COMPRA    
    registrarCompra$ = createEffect( () =>
    this.actions$.pipe(
        ofType(fromCompraActions.registrarCompra),
        mergeMap( action =>
            this.compraService.registrar(action.compra).pipe(
                map( compra => {
                    this.messageService.add({severity: 'success', summary: 'Success', detail: 'Se registro producto con éxito'});                    
                    return fromCompraActions.registarCompraSucess({compra});
                }),
                catchError( error => {
                    this.messageService.add({severity: 'error', summary: 'Error', detail: 'Error al Registar Producto'});
                    return of(fromCompraActions.registrarCompraFail({error}));
                })
            ) 
        )
    )
)
}