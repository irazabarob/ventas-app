import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DetalleVenta } from '../models/detalle-venta';

@Injectable({
  providedIn: 'root'
})
export class DetalleVentaService {

  constructor(private http: HttpClient) { }

  urlBase = "https://jesig.herokuapp.com/magneto-jlig";

  // registrarDetalleVenta(detalleVenta: DetalleVenta){
  //   return this.http.post<DetalleVenta>(this.urlBase + "/venta-")
  // }
}
