import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  urlBase = "https://jesig.herokuapp.com/magneto-jlig";
  // urlBase = "http://localhost:9090/magneto-jlig";

  constructor(private http: HttpClient) { }


  reporteMasVendidoMensual(params: any){
    return this.http.post<any>(`${this.urlBase}/reportes/venta-mensual/`, params);
  }

  reporteVentasGananciaMensual(anio: number){
    return this.http.post<any>(`${this.urlBase}/reportes/venta-ganancia-mensual`, anio);
  }

}
