import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Producto } from '../models/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  // urlBase = "https://jesig.herokuapp.com/magneto-jlig";
  urlBase = "http://localhost:9090/magneto-jlig";

  constructor(private http: HttpClient) { }

  listarProducto(){
    return this.http.get<Producto[]>(this.urlBase + "/producto/lista")
  }

  eliminarProducto(id: number) {
    return this.http.delete<Producto>(this.urlBase + "/producto/eliminar/" + id);
  }

  registrarProduto(producto: Producto) {
    return this.http.post<Producto>(this.urlBase + "/producto/registrar", producto);
  }

  editarProducto(producto: Producto){    
    return this.http.put<Producto>(this.urlBase + "/producto/modificar", producto);
  }
}
