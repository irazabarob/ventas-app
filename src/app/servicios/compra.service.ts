import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Compra } from '../models/compra';

@Injectable({
  providedIn: 'root'
})
export class CompraService {

  constructor(private http: HttpClient) { }

  urlBase = "https://jesig.herokuapp.com/magneto-jlig";
  // urlBase = "http://localhost:9090/magneto-jlig";

  registrar(compra: Compra){
    return this.http.post<Compra>(`${this.urlBase}/compras`, compra);
  }

}
