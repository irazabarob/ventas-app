import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReporteService {

  constructor(private http: HttpClient) { }

  urlBase = "https://jesig.herokuapp.com/magneto-jlig";
  // urlBase = "http://localhost:9090/magneto-jlig";

  reportePorMes(param: any){
    return this.http.post(`${this.urlBase}/reportes/ventas-al-mes`, param);
  }
}
