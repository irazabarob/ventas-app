import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DetalleVenta } from '../models/detalle-venta';
import { Venta } from '../models/venta';

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  constructor(private http: HttpClient) { }

  urlBase = "https://jesig.herokuapp.com/magneto-jlig";
// urlBase = "http://localhost:9090/magneto-jlig";


  registrarVenta(venta: Venta){ 
    return this.http.post<Venta>(this.urlBase + "/venta/registrar", venta);
  }

  registrarDV(detalleVenta: DetalleVenta[]){
    return this.http.post<DetalleVenta[]>(this.urlBase + "/venta/detalle-venta/registrar", detalleVenta);
  }
}
