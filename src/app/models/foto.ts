import { Producto } from './producto';

export class Foto {
    fotoId?: number;
    producto?: Producto;
    foto? : string;
}