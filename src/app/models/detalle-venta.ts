export interface DetalleVenta {
    idDetalle: number;
    idVenta: number;
    idProducto: number;
    precioFinal: number;
    cantidad: number;
}