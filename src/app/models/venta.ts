export interface Venta {
    idVenta?: number;
    idCliente: number;
    fecha: string;
}