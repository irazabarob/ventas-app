import { Producto } from "./producto";

export interface Compra {
    compraId?: number;
    fecha: Date;
    listProducto: Array<Producto>;
}