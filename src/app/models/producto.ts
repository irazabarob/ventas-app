import { Foto } from "./foto";

export interface Producto {
    idProducto: number;
    codigo: string;
    descripcion: string;
    marca: string;
    stock: number;
    precioCompra: number;
    precioVenta: number;
    foto: string;
    listFotos: Array<Foto>;
}