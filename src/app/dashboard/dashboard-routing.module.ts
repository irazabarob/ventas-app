import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MensualComponent } from './mensual/mensual.component';

const routes: Routes = [
  { path: "", component: MensualComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
