import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { MensualComponent } from './mensual/mensual.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [MensualComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule
  ]
})
export class DashboardModule { }
