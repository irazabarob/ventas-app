import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { element } from 'protractor';
import { DashboardService } from 'src/app/servicios/dashboard.service';


@Component({
  selector: 'app-mensual',
  templateUrl: './mensual.component.html',
  styleUrls: ['./mensual.component.scss']
})
export class MensualComponent implements OnInit {

  ctx: any;

  constructor(private dashboardService: DashboardService) { }

  ngOnInit(): void {
    this.cargarReporteMasVendidos();
    this.cargarReporteVentasGanancia();
  }

  cargarReporteMasVendidos() {
    let fecha = new Date();   
    this.dashboardService.reporteMasVendidoMensual({ mes: fecha.getMonth() + 1, anio: fecha.getFullYear() }).subscribe((data) => {
      this.vendidosPorMes(data);
    });
  }

  cargarReporteVentasGanancia() {
    let fecha = new Date();  
    this.dashboardService.reporteVentasGananciaMensual( fecha.getFullYear()).subscribe(data => {
      this.ventaGananciaMensual(data);
      this.gananciasMensualLineas(data);
    });
  }


  vendidosPorMes(data: Array<any>) {  
    let myChart = new Chart("myChart", {
      type: 'pie',
      data: {
        labels: data.map(el => el.CODIGO),
        datasets: [{
          data: data.map(el => el.vendidos),
          backgroundColor: [
            'rgba(255, 99, 132, 0.8)',
            'rgba(54, 162, 235, 0.8)',
            'rgba(122, 206, 11 , 0.8)',
            'rgba(223, 111, 12, 0.8)',
            'rgba(153, 102, 255, 0.8)',
            'rgba(255, 159, 64, 0.8)',
            'rgba(255, 206, 86, 0.8)',
            'rgba(75, 192, 192, 0.8)',
            'rgba(200, 76, 211, 0.8)',
            'rgba(22, 121, 22, 0.8)'
          ],
          borderColor: [
            'rgba(255, 99, 132)',
            'rgba(54, 162, 235)',
            'rgba(122, 206, 11 )',
            'rgba(223, 111, 12)',
            'rgba(153, 102, 255)',
            'rgba(255, 159, 64)',
            'rgba(255, 206, 86)',
            'rgba(75, 192, 192)',
            'rgba(200, 76, 211)',
            'rgba(22, 121, 22)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        legend: {
          display: true,
          position: 'right',
          labels: {
            fontColor: 'rgb(255, 99, 132)',
            fontSize: 16
          }
        }
      }
    });
  }

  ventaGananciaMensual(data: Array<any>) {  
    let myChart = new Chart("ventaGananciaChart", {
      type: 'bar',
      data: {
        labels: this.convertirAmesString(data),
        datasets: [this.dataVentas(data, true),this.dataGanancia(data, true)]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  gananciasMensualLineas(data : Array<any>){
    let myLineChart = new Chart("myLineChart", {
      type: 'line',      
      data: {
        labels: this.convertirAmesString(data),
        datasets: [this.dataVentas(data, false), this.dataGanancia(data, false)]        
      },
      // options: options
  });
  }

  

  dataVentas(data: Array<any>, colores: boolean){
    let ventas = {
      label: 'Ventas',
      data: data.map(el => el.total),          
      backgroundColor: colores? this.coloresVenta : "",
      fill: colores? true : false,
      borderColor: [
        'rgba(0, 111, 255, 1)'       
      ],
      borderWidth: 1
    }

    return ventas;
  }

  dataGanancia(data: Array<any>, colores: boolean){
    let ganancia = {
      label: 'Compras',
      data: data.map(el => el.ganancia),          
      backgroundColor: colores? this.coloresGanancia : "",
      fill: colores? true: false,
      borderColor: [
        'rgba(111, 223, 132, 1)' 
      ],
      borderWidth: 1
    }

    return ganancia;
  }

  convertirAmesString(data: Array<any>) {
    let meses = data.map( el => {
      switch (el.mes) {
        case 1:
          return "Ene"
          break;
        case 2:
          return "Feb"
          break;
        case 3:
          return"Mar"
          break;
        case 4:
          return "Abr"
          break;
        case 5:
          return "May"
          break;
        case 6:
          return "Jun"
          break;
        case 7:
          return "Jul"
          break;
        case 8:
          return "Ago"
          break;
        case 9:
          return "Set"
          break;
        case 10:
          return"Oct"
          break;
        case 11:
          return "Nov"
          break;
        case 12:
          return "Dic"
          break;
          default:
            return ""
            break;
      }
    })
  
    return meses;
  }

  coloresGanancia(){
    return [
      'rgba(111, 223, 132, 0.4)',    
      'rgba(111, 223, 132, 0.4)',    
      'rgba(111, 223, 132, 0.4)',    
      'rgba(111, 223, 132, 0.4)',    
      'rgba(111, 223, 132, 0.4)',    
      'rgba(111, 223, 132, 0.4)',    
      'rgba(111, 223, 132, 0.4)',    
      'rgba(111, 223, 132, 0.4)',    
      'rgba(111, 223, 132, 0.4)',    
      'rgba(111, 223, 132, 0.4)',    
      'rgba(111, 223, 132, 0.4)',    
      'rgba(111, 223, 132, 0.4)',    
    ]
  }

  coloresVenta(){
    return [
      'rgba(0, 111, 255, 0.4)',     
      'rgba(0, 111, 255, 0.4)',     
      'rgba(0, 111, 255, 0.4)',     
      'rgba(0, 111, 255, 0.4)',     
      'rgba(0, 111, 255, 0.4)',     
      'rgba(0, 111, 255, 0.4)',     
      'rgba(0, 111, 255, 0.4)',     
      'rgba(0, 111, 255, 0.4)',     
      'rgba(0, 111, 255, 0.4)',     
      'rgba(0, 111, 255, 0.4)',     
      'rgba(0, 111, 255, 0.4)',     
      'rgba(0, 111, 255, 0.4)',     
    ]
  }

}
