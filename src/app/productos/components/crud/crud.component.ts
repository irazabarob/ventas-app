import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, mergeMap, catchError, concatMap, finalize } from "rxjs/operators";
import { Producto } from 'src/app/models/producto';
import { Foto } from 'src/app/models/foto';
import { ProductoState } from '../../store/producto.reducer';
import * as fromProductoActions from "../../store/producto.actions";
import { selectProductos, selectShowModal } from '../../store/producto.selector';
import { ConfirmationService, MessageService, PrimeNGConfig } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseStorageService } from 'src/app/servicios/firebase-storage.service';
import { ProductoEffects } from '../../store/producto.effects';


@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss'],
  providers: [ConfirmationService]
})

export class CrudComponent implements OnInit {

  uploadedFiles: any[] = [];
  multiple: boolean = true;

  //variables para subir imagenes a firebase storage
  public nombreArchivo = '';
  public URLPublica = '';
  public porcentaje = 0;
  public cargando = false;
  fotoEnviar: any;

  //variables para modal
  displayModal: boolean = false;
  addProdBoolean: boolean = false;
  editProdBoolean: boolean = false;

  productos$: Observable<Producto[]> = new Observable();
  productoEditar!: Producto;

  productoForm = new FormGroup({});

  //variables para fotos
  listFoto: Array<File> = [];
  urlFotos: Array<string> = [];

  constructor(private confirmationService: ConfirmationService,
    private productoStore: Store<ProductoState>,
    private primengConfig: PrimeNGConfig, //no se para que es, pero vino con los toast.
    private messageService: MessageService,
    private fb: FormBuilder,
    private firebaseStorage: FirebaseStorageService) { }

  ngOnInit(): void {
    this.inicializarForm();
    this.primengConfig.ripple = true;
    this.productoStore.dispatch(fromProductoActions.cargarProductos());
    this.cargarProductos();
  }

  inicializarForm() {
    this.productoForm = this.fb.group({
      idProducto: [""],
      codigo: ["", Validators.required],
      descripcion: ["", Validators.required],
      marca: ["", Validators.required],
      precioCompra: ["", [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      precioVenta: ["", [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      stock: ["", [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      foto: [""]
    })
  }

  cargarProductos() {
    this.productos$ = this.productoStore.pipe(select(selectProductos));
  }

  eliminarProd(id: number) {
    this.confirmationService.confirm({
      message: 'Estas seguro de eliminar este Producto?',
      header: 'Confirmar Eliminación',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.productoStore.dispatch(fromProductoActions.eliminarProducto({ id }));
      }
    });
  }

  showModalDialog(producto?: Producto) {
    if (producto != null) {
      this.editProdBoolean = true;
      this.productoEditar = producto;
      this.cargarProductoEditar();
    } else {
      this.limpiarForm();
      this.addProdBoolean = true;
    }

    this.displayModal = true;
    this.productoStore.dispatch(fromProductoActions.mostrarModal({ mostrar: true }));
  }

  cargarProductoEditar() {
    this.productoForm.controls.idProducto.setValue(this.productoEditar.idProducto);
    this.productoForm.controls.codigo.setValue(this.productoEditar.codigo);
    this.productoForm.controls.descripcion.setValue(this.productoEditar.descripcion);
    this.productoForm.controls.marca.setValue(this.productoEditar.marca);
    this.productoForm.controls.precioCompra.setValue(this.productoEditar.precioCompra);
    this.productoForm.controls.precioVenta.setValue(this.productoEditar.precioVenta);
    this.productoForm.controls.stock.setValue(this.productoEditar.stock);
    // this.productoForm.controls.foto.setValue(this.productoEditar.foto);   
  }

  addOrEdit() {
    if (this.addProdBoolean) {
      this.registrarProducto();
    }
    if (this.editProdBoolean) {
      this.editarProducto();
    }
  }

  async registrarProducto() { 
    let producto: Producto = this.productoForm.value;
    producto.listFotos = [];   
   
    let count = 0;
    for (let file of this.listFoto) {
      let referencia = this.firebaseStorage.referenciaCloudStorage(file.name);
      let tarea = this.firebaseStorage.tareaCloudStorage(file.name, file);

      tarea.snapshotChanges().pipe(
        finalize(() => {
          referencia.getDownloadURL().subscribe(downloadURL => {
            count++;
            this.obtenerUrl(downloadURL);

            if (this.listFoto.length === count) {
              for (let url of this.urlFotos) {
                let f = new Foto();
                f.foto = url;
                producto.listFotos.push(f);
              }
       
              this.productoStore.dispatch(fromProductoActions.registrarProducto({ producto }));

              this.productoStore.pipe(select(selectShowModal)).subscribe(show => {
                this.displayModal = show!;
                this.limpiarForm();
              })
            }

          });
        })
      ).subscribe();
    }
    
  }

  obtenerUrl(url: string) {
    this.urlFotos.push(url);
  }

  editarProducto() {
    let producto = this.productoForm.value;

    if (this.fotoEnviar === null || this.fotoEnviar === undefined || this.fotoEnviar === "") {
      producto.foto = this.productoEditar.foto;
      this.productoStore.dispatch(fromProductoActions.editarProducto({ producto }));
      this.cerrarModal();
      return;
    }

    this.cargando = true;
    let referencia = this.firebaseStorage.referenciaCloudStorage(this.nombreArchivo);
    let tarea = this.firebaseStorage.tareaCloudStorage(this.nombreArchivo, this.fotoEnviar);

    // Cambia el porcentaje
    this.porcentaje = 0;
    tarea.percentageChanges().subscribe((porcentaje) => {
      this.porcentaje = Math.round(porcentaje!);

      if (this.porcentaje === 100) {
        tarea.snapshotChanges().pipe(
          finalize(() => {
            producto.foto = referencia.getDownloadURL().subscribe(URL => {
              if (this.cargando === true) {
                producto.foto = URL;
                this.productoStore.dispatch(fromProductoActions.editarProducto({ producto }));
                this.cerrarModal();
              }
            })
          })
        ).subscribe();

      }
    });
  }

  selectImage(event: any) {   
    if (event.target.files) {
      let reader = new FileReader();

      this.nombreArchivo = event.target.files[0].name;
      this.fotoEnviar = event.target.files[0];

      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (e: any) => {
        this.URLPublica = e.target.result;
      }
    }
  }

  onUpload(e: any) {   
    this.listFoto = e.currentFiles;
  }

  cerrarModal() {
    this.productoStore.pipe(select(selectShowModal)).subscribe(show => {
      this.displayModal = show!;
    })
  }

  limpiarForm() {
    this.displayModal = false;
    this.cargando = false;
    this.fotoEnviar = "";
    this.URLPublica = "";

    this.inicializarForm();

    this.editProdBoolean = false;
    this.addProdBoolean = false;
    this.productoForm.reset();
    this.listFoto = [];
  }



}
