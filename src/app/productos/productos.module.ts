import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductosRoutingModule } from './productos-routing.module';
import { CrudComponent } from './components/crud/crud.component';

// PRIME NG
import {TableModule} from 'primeng/table';

// STORE NGRX
import { StoreModule } from '@ngrx/store';
import { productoReducer, productoStateFeatureKey } from './store/producto.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ProductoEffects } from './store/producto.effects';

//PRIME NG
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import {ToastModule} from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import {FileUploadModule} from 'primeng/fileupload';
import {ProgressBarModule} from 'primeng/progressbar';

@NgModule({
  declarations: [CrudComponent],
  imports: [
    CommonModule,
    ProductosRoutingModule,
    TableModule,
    ToastModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    ConfirmDialogModule,
    ReactiveFormsModule,
    ProgressBarModule,
    FileUploadModule,
    StoreModule.forFeature(productoStateFeatureKey, productoReducer),
    EffectsModule.forFeature([ProductoEffects])
  ],
  exports: [
    CrudComponent
  ],
  providers: [MessageService]
})
export class ProductosModule { }
