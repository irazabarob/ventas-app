import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ProductoState, productoStateFeatureKey } from "./producto.reducer";

export const selectProductosFeature = createFeatureSelector<ProductoState>(
    productoStateFeatureKey
)

export const selectProductos = createSelector(
    selectProductosFeature,
    (state: ProductoState) => state.productos
)

export const selectShowModal = createSelector(
    selectProductosFeature,
    (state: ProductoState) => state.showModal
)