import { Action, createReducer, on } from "@ngrx/store";
import { Producto } from "src/app/models/producto";

import * as productoActions from "./producto.actions";

export const productoStateFeatureKey = "productoState";

export interface ProductoState {
    productos: Producto[];
    showModal?: boolean;
    error: any;
}

export const initialState: ProductoState = {
    productos: [],
    showModal: true,
    error: undefined
}

export const _productoReducer = createReducer(
    initialState,
    on(productoActions.cargarProductosSuccess, (state,action) => {       
        return {
            productos: action.productos,           
            error: undefined
        }
    }),
    on(productoActions.cargarProductosFail, (state,action) => {
        return {
            productos: state.productos,          
            error: action.error
        }
    }),
    on(productoActions.registrarProductoSuccess, (state, action) => {
        return {
            productos: [...state.productos, action.producto],
            showModal: false,
            error: undefined
        }
    }),
    on(productoActions.editarProductoSuccess, (state, action) => {
        let productos = state.productos.map(p => {
            if(p.idProducto == action.producto.idProducto){
                p = action.producto;
            }
            return p;
        })
        return {
            productos: productos,
            showModal: false,
            error: undefined
        }
    }),
    on(productoActions.eliminarProductoSuccess, (state, action) => {
        let listaProducto = state.productos.filter(p => p.idProducto != action.id);
        return {
            productos: listaProducto,            
            error: undefined
        }
    }),
    on(productoActions.mostrarModal, (state, action) => {
        return {
            productos: state.productos,
            showModal: action.mostrar,
            error: undefined
        }
    })
)

export function productoReducer(state: ProductoState | undefined, action: Action){
    return _productoReducer(state, action);
}