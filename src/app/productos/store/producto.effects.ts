import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, mergeMap, catchError, concatMap } from "rxjs/operators";
import { of } from "rxjs";
import { ProductoService } from "src/app/servicios/producto.service";

import * as fromProductoActions from "./producto.actions";
import { MessageService } from "primeng/api";
import { Producto } from "src/app/models/producto";

@Injectable()
export class ProductoEffects {
    
    constructor(private actions$: Actions, private productoService: ProductoService,
                private messageService: MessageService){}

    //CARGAR PRODUCTOS    
    loadProducts$ = createEffect( () => 
        this.actions$.pipe(
            ofType(fromProductoActions.cargarProductos),
            mergeMap( () => 
                this.productoService.listarProducto().pipe(
                    map((productos: Producto[]) => {                       
                        return fromProductoActions.cargarProductosSuccess({productos})
                    }),
                    catchError(error => of(fromProductoActions.cargarProductosFail({error}))
                )
            )
        )
    ));

    //REGISTRAR PRODUCTO
    registrarProducto$ = createEffect( () =>
        this.actions$.pipe(
            ofType(fromProductoActions.registrarProducto),
            mergeMap( action =>
                this.productoService.registrarProduto(action.producto).pipe(
                    map( producto => {
                        this.messageService.add({severity: 'success', summary: 'Success', detail: 'Se registro producto con éxito'});
                        fromProductoActions.mostrarModal({mostrar: false});
                        return fromProductoActions.registrarProductoSuccess({producto});
                    }),
                    catchError( error => {
                        this.messageService.add({severity: 'error', summary: 'Error', detail: 'Error al Registar Producto'});
                        return of(fromProductoActions.cargarProductosFail({error}));
                    })
                ) 
            )
        )
    )

    //MODIFICAR PRODUCTO
    editarProducto$ = createEffect( () =>
        this.actions$.pipe(
            ofType(fromProductoActions.editarProducto),
            mergeMap( action =>
                this.productoService.editarProducto(action.producto).pipe(
                    map( producto => {
                        this.messageService.add({severity: 'success', summary: 'Success', detail: 'Se actualizo producto con éxito'});
                        fromProductoActions.mostrarModal({mostrar: false});
                        return fromProductoActions.editarProductoSuccess({producto})
                    }),
                    catchError( error => {
                        this.messageService.add({severity: 'error', summary: 'Error', detail: 'Error al editar Producto'});
                        return of(fromProductoActions.cargarProductosFail({error}))
                    })
                )
            )
        )
    )

    //ELIMINAR PRODUCTO
    eliminarProducto$ = createEffect( () => 
        this.actions$.pipe(
            ofType(fromProductoActions.eliminarProducto),
            mergeMap( action => 
                this.productoService.eliminarProducto(action.id).pipe(
                    map( () => {
                        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Se Elimino Producto con éxito' });
                        return fromProductoActions.eliminarProductoSuccess({id: action.id});
                    }),
                    catchError( error => {
                        this.messageService.add({severity:'error', summary: 'Error', detail: 'Error al Eliminar Producto'});
                        return of(fromProductoActions.eliminarProductoFail({error}));
                    })
                )   
            )
        )
    );

}