import { createAction, props } from "@ngrx/store";
import { Producto } from "src/app/models/producto";
import { ProductoEffects } from "./producto.effects";

// carga de productos
export const cargarProductos = createAction(
    "[Producto] Cargar Productos"   
)

export const cargarProductosSuccess = createAction(
    "[Producto] Cargar Productos Success",
    props<{productos: Producto[]}>()
)

export const cargarProductosFail = createAction(
    "[Producto] Cargar Productos Fail",
    props<{error: any}>()
)

//agregar nuevo producto
export const registrarProducto = createAction(
    "[Producto] Registrar Producto",
    props<{producto: Producto}>()
)

export const registrarProductoSuccess = createAction(
    "[Producto] Registrar Producto Success",
    props<{producto: Producto}>()
)

export const registrarProductoFail = createAction(
    "[Producto] Registrar Producto Fail",
    props<{error: any}>()
)
//actualizar producto
export const editarProducto = createAction(
    "[Producto] Editar Producto",
    props<{producto: Producto}>()
)

export const editarProductoSuccess = createAction(
    "[Producto] Editar Producto Success",
    props<{producto: Producto}>()
)

export const editarProductoFail = createAction(
    "[Producto] Editar Producto Fail",
    props<{error: any}>()
)
//eliminar producto
export const eliminarProducto = createAction(
    "[Producto] Eliminar Producto",
    props<{id: number}>()
)

export const eliminarProductoSuccess = createAction(
    "[Producto] Eliminar Producto Success",
    props<{id: number}>()
)

export const eliminarProductoFail = createAction(
    "[Producto] Eliminar Producto Fail",
    props<{error: any}>()
)

//SHOW MODAL PRODUCTO
export const mostrarModal = createAction(
    "[Producto] Mostrar Modal Registrar",
    props<{mostrar: boolean}>()
)