import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VentasRoutingModule } from './ventas-routing.module';
import { VentaComponent } from './venta/venta.component';

//PRIME NG
import {DropdownModule} from 'primeng/dropdown';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { StoreModule } from '@ngrx/store';
import { ventaReducer, ventaStateFeatureKey } from './store/ventas.reducer';
import { EffectsModule } from '@ngrx/effects';
import { VentaEffects } from './store/ventas.effects';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';

@NgModule({
  declarations: [VentaComponent ],
  imports: [
    CommonModule,
    VentasRoutingModule,
    DropdownModule,
    ReactiveFormsModule,
    ButtonModule,
    ToastModule,
    TableModule,
    StoreModule.forFeature(ventaStateFeatureKey, ventaReducer),
    EffectsModule.forFeature([VentaEffects]),
  ],
  providers: [MessageService]
})
export class VentasModule { }
