import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { SelectItem } from 'primeng/api';
import { Observable } from 'rxjs';
import { concatMap } from 'rxjs/operators';
import { DetalleVenta } from 'src/app/models/detalle-venta';
import { Producto } from 'src/app/models/producto';
import { Venta } from 'src/app/models/venta';
import { selectProductos } from 'src/app/productos/store/producto.selector';
import { VentaService } from 'src/app/servicios/venta.service';

//store
import * as fromProductoActions from "../../productos/store/producto.actions";
import * as fromVentaActions from "../store/ventas.actions";

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.scss']
})
export class VentaComponent implements OnInit {

  productos: SelectItem[] = [];
  productoSeleccionado!: SelectItem;
  listProducto!: Producto[];

  ventaTotal: number = 0;
  //variables para preparar data a vender
  productosAVender: Producto[] = [];
  listaVenta: DetalleVenta[] = [];


  productoForm: FormGroup = new FormGroup({});

  constructor(private productoStore: Store<Producto>,
    private ventaStore: Store<Venta>,
    private fb: FormBuilder,
    private ventaservice: VentaService) { }

  ngOnInit(): void {
    this.cargarDatosProductos();
    this.inicializarFormProd();
  }

  inicializarFormProd() {
    this.productoForm = this.fb.group({
      idProducto: [""],
      codigo: [""],
      descripcion: [""],
      marca: [""],
      stock: [""],
      precioVenta: [""],
      cantidad: [""]
    })
  }

  cargarDatosProductos() {
    this.productoStore.pipe(select(selectProductos)).subscribe(listProduct => {
      this.listProducto = listProduct;
      listProduct.forEach(p => {
        this.productos.push({ label: p.codigo, value: p.idProducto })
      })
    })
  }

  select() {
    let prod = this.listProducto.filter(p => this.productoForm.controls.idProducto.value == p.idProducto);

    this.productoForm.controls.idProducto.setValue(prod[0].idProducto);
    this.productoForm.controls.codigo.setValue(prod[0].codigo);
    this.productoForm.controls.precioVenta.setValue(prod[0].precioVenta);
    this.productoForm.controls.stock.setValue(prod[0].stock);
    this.productoForm.controls.descripcion.setValue(prod[0].descripcion);
  }

  addToVenta() {
    let prod = this.productoForm.value;
    this.productosAVender.push(prod);

    this.ventaTotal += prod.precioVenta * prod.cantidad;

    //añadimos productoss a la lista detalle de venta
    let dv = {
      idProducto: prod.idProducto,
      precioFinal: prod.precioVenta,
      cantidad: prod.cantidad
    } as DetalleVenta

    this.listaVenta.push(dv);
    this.inicializarFormProd();
  }

  registarVenta() {
    let fech = new Date();
    let venta = {
      idCliente: 1,
      fecha: `${fech.getFullYear()}-${fech.getMonth() + 1}-${fech.getDate()}`
    };

    this.ventaStore.dispatch(fromVentaActions.registrarVenta({ venta: venta as Venta, dv: this.listaVenta }));

    setTimeout(() => {
      this.inicializarFormProd();
      this.productosAVender = [];
      this.listaVenta = [];
    },500);
  }

}
