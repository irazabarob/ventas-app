import { Action, createReducer, on } from "@ngrx/store";
import { Venta } from "src/app/models/venta";

import * as fromVentaActions from "./ventas.actions";

export const ventaStateFeatureKey = "ventasState";

export interface VentasState {
    ventas: Venta[];
    error: any;
}

export const initialState: VentasState = {
    ventas: [],    
    error: undefined
}

export const _ventaReducer = createReducer(
    initialState,
    on(fromVentaActions.registrarVentaSuccess, (state, action) => {
        return {
            ventas: [...state.ventas, action.venta],
            error: undefined
        }
    })
)

export function ventaReducer(state: VentasState | undefined, action: Action){
    return _ventaReducer(state, action);
}
