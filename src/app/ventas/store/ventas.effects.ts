import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { MessageService } from "primeng/api";
import { of } from "rxjs";
import { dispatch } from "rxjs/internal/observable/pairs";
import { catchError, map, mergeMap } from "rxjs/operators";
import { VentaService } from "src/app/servicios/venta.service";

import * as fromVentaActions from "./ventas.actions";

@Injectable()
export class VentaEffects{

    constructor(private actions$: Actions,
                private ventaService: VentaService,
                private messageService: MessageService){}


    registrarVenta$ = createEffect( () =>
        this.actions$.pipe(
            ofType(fromVentaActions.registrarVenta),
            mergeMap( action =>
                this.ventaService.registrarVenta(action.venta).pipe(
                    map(venta => {   
                        this.ventaService.registrarDV(action.dv).subscribe( () => {
                            this.messageService.add({severity: 'success', summary: 'Success', detail: 'Se registro venta con éxito'});
                        });                                          
                        return fromVentaActions.registrarVentaSuccess({venta});
                    }),
                    catchError(error => {
                        this.messageService.add({severity: 'error', summary: 'Error', detail: 'Error al Registar Venta'});
                        return of(fromVentaActions.registrarVentaFail({error}));
                    })
                )
            )
        )
    )

    registrarDetalleVenta$ = createEffect( () =>
        this.actions$.pipe(
            ofType(fromVentaActions.registrarDetalleVenta),
            mergeMap( action =>
                this.ventaService.registrarDV(action.detalleVenta).pipe(
                    map( detalleVenta => {
                        return fromVentaActions.registrarDetalleVentaSuccess({detalleVenta});
                    }),
                    catchError( error => {
                        return of(fromVentaActions.registrarVentaFail({error}));
                    })
                )
            )
        )
    )
}