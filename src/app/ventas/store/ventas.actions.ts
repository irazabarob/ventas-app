import { createAction, props } from "@ngrx/store";
import { Venta } from "src/app/models/venta";
import { DetalleVenta } from "src/app/models/detalle-venta";

export const registrarVenta = createAction(
    "[Ventas] Registrar Venta",
    props<{venta: Venta, dv: DetalleVenta[]}>()
)

export const registrarVentaSuccess = createAction(
    "[Ventas] Registrar Venta Success",
    props<{venta: Venta}>()
)

export const registrarVentaFail = createAction(
    "[Ventas] Registrar Venta Fail",
    props<{error: any}>()
)

//REGISTRAR DETALLE VENTA
export const registrarDetalleVenta = createAction(
    "[Ventas] Registrar Detalle Venta",
    props<{detalleVenta: DetalleVenta[]}>()
)

export const registrarDetalleVentaSuccess = createAction(
    "[Ventas] Registrar Detalle Venta Success",
    props<{detalleVenta: DetalleVenta[]}>()
)

export const registrarDetalleVetanFail = createAction(
    "[Ventas] Registrar Detalle Venta Fail",
    props<{error: any}>()
)