import { createFeatureSelector, createSelector } from "@ngrx/store"
import { VentasState, ventaStateFeatureKey } from "./ventas.reducer"

export const selectVentasFeature = createFeatureSelector<VentasState>(
    ventaStateFeatureKey
)

export const selectProductos = createSelector(
    selectVentasFeature,
    (state: VentasState) => state.ventas
)