import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: "productos",
    loadChildren: () => import("./productos/productos.module").then(m => m.ProductosModule)
  },
  {
    path: "ventas",
    loadChildren: () => import("./ventas/ventas.module").then(m => m.VentasModule)
  },
  {
    path: "compras",
    loadChildren: () => import("./compras/compras.module").then(m => m.ComprasModule)
  },
  {
    path: "dashboard",
    loadChildren: () => import("./dashboard/dashboard.module").then(m => m.DashboardModule)
  },
  {
    path: "reportes",
    loadChildren: () => import("./reportes/reportes.module").then(m => m.ReportesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
