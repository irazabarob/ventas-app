// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDvSZWBRoKalA16NGJ5tuG-PcFXF7aivcM",
    authDomain: "magneto-jlig.firebaseapp.com",
    databaseURL: "https://magneto-jlig.firebaseio.com",
    projectId: "magneto-jlig",
    storageBucket: "magneto-jlig.appspot.com",
    messagingSenderId: "14354661584",
    appId: "1:14354661584:web:f7a8bb033af006e1fd5ed5"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
